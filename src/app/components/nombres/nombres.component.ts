import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-nombres',
  templateUrl: './nombres.component.html',
  styleUrls: ['./nombres.component.scss']
})


export class NombresComponent implements OnInit {

  form!: FormGroup;
  form2!: FormGroup;
  GrupoNombres: string[] = [];
  AgregarNombres!: string;
  seleccione: string = 'Seleccione';

  constructor(private fb: FormBuilder) {
    this.Formulario();
    this.Formulario2();
  }

  ngOnInit(): void {
  }



  Formulario(): void {
    this.form = this.fb.group({
      nombre: ['', [Validators.required, Validators.pattern(/[\Sa-zA-ZñN\S]/)]],
    })
  }

  Formulario2(): void {
    this.form2 = this.fb.group({
      nombres: ['', Validators.required],
      Nombres: ['']
    })
  }


  get nombreNoValido() {
    return this.form.get('nombre')?.invalid && this.form.get('nombre')?.touched
  }



  AnadidoNombre(): void {

    this.form2.value.nombres = this.form.value.nombre.trim();

    this.GrupoNombres.push(this.form2.value.nombres)

    this.form.reset();

  }


  ImprimirNombres(): void {

    this.form2.value.Nombres = this.GrupoNombres.join('\n');

    this.AgregarNombres = this.form2.value.Nombres

    console.log(this.AgregarNombres);

  }

}
